// Import Modules
import { spmndActor } from "./actor/actor.js";
import { spmndActorSheet } from "./actor/actor-sheet.js";
import { spmndItem } from "./item/item.js";
import { spmndItemSheet } from "./item/item-sheet.js";

Hooks.once('init', async function() {

  game.splittermond = {
    spmndActor,
    spmndItem
  };

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "1d20",
    decimals: 2
  };

  // Define custom Entity classes
  CONFIG.Actor.entityClass = spmndActor;
  CONFIG.Item.entityClass = spmndItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("splittermond", spmndActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("splittermond", spmndItemSheet, { types: ["item","resource","strength","weakness","language","culture"], makeDefault: true });
  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });
});
import { SpmndRollDialog } from './spmndroll-dialog.js';

export const SpmndRollTypes  = Object.freeze({
  StandardRoll: 'Splittermond.RollTypes.StandardRoll',
  SafetyRoll: 'Splittermond.RollTypes.SafetyRoll',
  RiskyRoll: 'Splittermond.RollTypes.RiskyRoll'});

export class SpmndRoll {

  static roll(title, roll, event) {
      new SpmndRollDialog(title, roll).render(true);
  }
}

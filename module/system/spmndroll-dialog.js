import { SpmndRollTypes } from "./spmndroll.js";

export class SpmndRollDialog extends Application {

    constructor(title,roll) {
        super({
            title: title,
            template: 'systems/splittermond/templates/system/spmndroll-dialog.html',
            popOut: true,
            width: 380,
        });
        this.rollType = $("#rollTypes option:selected")[0].value;
        $('#rollButton').text(this.rollType);
    };

    getData() {
        const sheetData = super.getData();

        sheetData.SpmndRollTypes = SpmndRollTypes;

        return sheetData;
    }

    activateListeners(html) {
        html.find('#rollButton').click((event) => {
          // TODO:
          this.close();
        });

        html.find('#rollTypes').change((event) => {
            this.rollType = $("#rollTypes option:selected")[0].value;
            if (this.rollType === SpmndRollTypes.StandardRoll)
            {
                
            }
            else if (this.rollType === SpmndRollTypes.SafetyRoll)
            {

            }
            else if (this.rollType === SpmndRollTypes.RiskyRoll)
            {
                
            }
            $('#rollButton').text(this.rollType);
        });
    }
}